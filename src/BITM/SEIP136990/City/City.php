<?php

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class City extends DB
{
    public $id;
    public $name;
    public $city;

   /* public function index(){
        echo "i am inside ".__CLASS__." class";
    }*/
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }
    public function setData($postVaribaleData=NULL)
    {
        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("name",$postVaribaleData))
        {
            $this->name = $postVaribaleData['name'];
        }
        if(array_key_exists("city_name",$postVaribaleData))
        {
            $this->city = $postVaribaleData['city_name'];
        }

    }//end of set data
    public function store()
    {
        $arrData = array($this->name,$this->city);
        $sql = "INSERT into city(name,city_name) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");

        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");

        Utility::redirect('createBK.php');


    }

}